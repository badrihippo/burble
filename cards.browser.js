(function(){function r(e,n,t){function o(i,f){if(!n[i]){if(!e[i]){var c="function"==typeof require&&require;if(!f&&c)return c(i,!0);if(u)return u(i,!0);var a=new Error("Cannot find module '"+i+"'");throw a.code="MODULE_NOT_FOUND",a}var p=n[i]={exports:{}};e[i][0].call(p.exports,function(r){var n=e[i][1][r];return o(n||r)},p,p.exports,r,e,n,t)}return n[i].exports}for(var u="function"==typeof require&&require,i=0;i<t.length;i++)o(t[i]);return o}return r})()({1:[function(require,module,exports){
"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.Card = Card;
exports.Deck = Deck;
exports.SUITS = exports.RANKS = void 0;
var _cryptoShuffle = _interopRequireDefault(require("crypto-shuffle"));
function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }
function _toConsumableArray(arr) { return _arrayWithoutHoles(arr) || _iterableToArray(arr) || _unsupportedIterableToArray(arr) || _nonIterableSpread(); }
function _nonIterableSpread() { throw new TypeError("Invalid attempt to spread non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method."); }
function _iterableToArray(iter) { if (typeof Symbol !== "undefined" && iter[Symbol.iterator] != null || iter["@@iterator"] != null) return Array.from(iter); }
function _arrayWithoutHoles(arr) { if (Array.isArray(arr)) return _arrayLikeToArray(arr); }
function _createForOfIteratorHelper(o, allowArrayLike) { var it = typeof Symbol !== "undefined" && o[Symbol.iterator] || o["@@iterator"]; if (!it) { if (Array.isArray(o) || (it = _unsupportedIterableToArray(o)) || allowArrayLike && o && typeof o.length === "number") { if (it) o = it; var i = 0; var F = function F() {}; return { s: F, n: function n() { if (i >= o.length) return { done: true }; return { done: false, value: o[i++] }; }, e: function e(_e) { throw _e; }, f: F }; } throw new TypeError("Invalid attempt to iterate non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method."); } var normalCompletion = true, didErr = false, err; return { s: function s() { it = it.call(o); }, n: function n() { var step = it.next(); normalCompletion = step.done; return step; }, e: function e(_e2) { didErr = true; err = _e2; }, f: function f() { try { if (!normalCompletion && it["return"] != null) it["return"](); } finally { if (didErr) throw err; } } }; }
function _unsupportedIterableToArray(o, minLen) { if (!o) return; if (typeof o === "string") return _arrayLikeToArray(o, minLen); var n = Object.prototype.toString.call(o).slice(8, -1); if (n === "Object" && o.constructor) n = o.constructor.name; if (n === "Map" || n === "Set") return Array.from(o); if (n === "Arguments" || /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(n)) return _arrayLikeToArray(o, minLen); }
function _arrayLikeToArray(arr, len) { if (len == null || len > arr.length) len = arr.length; for (var i = 0, arr2 = new Array(len); i < len; i++) { arr2[i] = arr[i]; } return arr2; }
var RANKS = ['A', 'K', 'Q', 'J', '10', '9', '8', '7', '6', '5', '4', '3', '2'];
exports.RANKS = RANKS;
var SUITS = ['S', 'H', 'D', 'C'];
exports.SUITS = SUITS;
function Deck(ranks, suits) {
  var _this = this;
  if (!ranks) this.ranks = [].concat(RANKS);else this.ranks = ranks;
  if (!suits) this.suits = [].concat(SUITS);else this.suits = suits;

  //packing a full deck of cards
  this.cards = [];
  var _iterator = _createForOfIteratorHelper(this.suits),
    _step;
  try {
    for (_iterator.s(); !(_step = _iterator.n()).done;) {
      var s = _step.value;
      var _iterator2 = _createForOfIteratorHelper(this.ranks),
        _step2;
      try {
        for (_iterator2.s(); !(_step2 = _iterator2.n()).done;) {
          var r = _step2.value;
          this.cards.push(new Card(s, r));
        }
      } catch (err) {
        _iterator2.e(err);
      } finally {
        _iterator2.f();
      }
    }

    // creating getter and setter for trump property
  } catch (err) {
    _iterator.e(err);
  } finally {
    _iterator.f();
  }
  this.trump = {
    set: function set(value) {
      //if the value is other than {undefined, S, H, C, D} then throw an error or else save it to trump property
      if ([].concat(_toConsumableArray(_this.suits), [undefined]).includes(value)) {
        _this.trump = value;
      } else {
        throw new TypeError("Invalid trump value: ".concat(value));
      }
    },
    get: function get() {
      return _this.trump;
    }
  };

  // flip value is either true or false, other than that will propmts an error
  this.flip = {
    set: function set(value) {
      if (value === true || value === false) {
        _this.flip = value;
      } else {
        throw TypeError("Invalid input value");
      }
    },
    get: function get() {
      return _this.flip;
    }
  };
  this.getCard = function (card) {
    if (!card) return;
    var cardFound;
    _this.cards.forEach(function (c, index) {
      if (c.suit === card.suit && c.rank === card.rank) {
        _this.cards.splice(index, 1);
        cardFound = card;
      }
    });
    return cardFound;
  };
  this.compare = function (c1, c2) {
    var s1 = _this.suits.indexOf(c1.suit);
    var s2 = _this.suits.indexOf(c2.suit);

    // return by suit value if different
    if (s1 != s2) {
      return s1 - s2;
    }

    // otherwise, suit must be same
    // compare by rank instead
    var r1 = _this.ranks.indexOf(c1.rank);
    var r2 = _this.ranks.indexOf(c2.rank);
    return r1 - r2;
  };

  // To deal the cards to each player hand
  this.deal = function (no_of_players, cardCount, cardsToPop) {
    var hands = [];
    var deck = _this.shuffle(_this.cards);

    // Check whether the given card count is out of box
    if (_this.cards.length < no_of_players * cardCount) {
      throw Error("Not enough cards to deal in the deck");
    }

    // First initialize an empty array
    var empty = [];
    for (var j = 0; j < no_of_players; j++) {
      for (var i = 0; i < cardCount; i++) {
        empty.push(deck.pop());
      }
      hands.splice(j, 0, empty);
      empty = [];
    }

    // Return array of hands and remaining
    return {
      hands: hands,
      remaining: deck
    };
  };
}

// Used a crypto-shuffle module to shuffle the cards
Deck.prototype.shuffle = function (deck) {
  return (0, _cryptoShuffle["default"])(deck);
};
function Card(suit, rank) {
  this.suit = suit.toUpperCase();
  this.rank = rank.toUpperCase();
}

},{"crypto-shuffle":4}],2:[function(require,module,exports){
"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;
var _deck = require("./deck.js");
var _trick = _interopRequireDefault(require("./trick.js"));
var _tricks = _interopRequireDefault(require("./tricks.js"));
function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }
var cards = {
  RANKS: _deck.RANKS,
  SUITS: _deck.SUITS,
  Deck: _deck.Deck,
  Card: _deck.Card,
  Trick: _trick["default"],
  Tricks: _tricks["default"]
};
if (typeof window !== "undefined" && typeof window.document !== "undefined") {
  // set variables for browser
  window.cards = cards;
}
var _default = cards;
exports["default"] = _default;

},{"./deck.js":1,"./trick.js":7,"./tricks.js":8}],3:[function(require,module,exports){

},{}],4:[function(require,module,exports){
var getRandomValues = require('get-random-values');

function shuffle(a) {
   var n = a.length,                   // The number of items left to shuffle (loop invariant)
       r = new Uint8Array(n),          // Some random values
        k, t;
    getRandomValues(r);
    while (n > 1) {
        k = r[n-1] % n;                 // 0 <= k < n
        t = a[--n];                     // swap elements n and k
        a[n] = a[k];
        a[k] = t;
    }
    return a;                           // for a fluent API
}

module.exports = shuffle;
},{"get-random-values":5}],5:[function(require,module,exports){
var window = require('global/window');
var nodeCrypto = require('crypto');

function getRandomValues(buf) {
  if (window.crypto && window.crypto.getRandomValues) {
    return window.crypto.getRandomValues(buf);
  }
  if (typeof window.msCrypto === 'object' && typeof window.msCrypto.getRandomValues === 'function') {
    return window.msCrypto.getRandomValues(buf);
  }
  if (nodeCrypto.randomBytes) {
    if (!(buf instanceof Uint8Array)) {
      throw new TypeError('expected Uint8Array');
    }
    if (buf.length > 65536) {
      var e = new Error();
      e.code = 22;
      e.message = 'Failed to execute \'getRandomValues\' on \'Crypto\': The ' +
        'ArrayBufferView\'s byte length (' + buf.length + ') exceeds the ' +
        'number of bytes of entropy available via this API (65536).';
      e.name = 'QuotaExceededError';
      throw e;
    }
    var bytes = nodeCrypto.randomBytes(buf.length);
    buf.set(bytes);
    return buf;
  }
  else {
    throw new Error('No secure random number generator available.');
  }
}

module.exports = getRandomValues;

},{"crypto":3,"global/window":6}],6:[function(require,module,exports){
(function (global){(function (){
var win;

if (typeof window !== "undefined") {
    win = window;
} else if (typeof global !== "undefined") {
    win = global;
} else if (typeof self !== "undefined"){
    win = self;
} else {
    win = {};
}

module.exports = win;

}).call(this)}).call(this,typeof global !== "undefined" ? global : typeof self !== "undefined" ? self : typeof window !== "undefined" ? window : {})
},{}],7:[function(require,module,exports){
"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = Trick;
function Trick() {
  var _this = this;
  this.play = function (_ref) {
    var player = _ref.player,
      card = _ref.card;
    _this.push({
      player: player,
      card: card
    });
  };
  this.flip = {
    set setFlip(value) {
      if (typeof value == "boolean") this.flip = value;else throw TypeError('flip should be a boolean');
    },
    get getFlip() {
      return this.flip;
    }
  };
  this.getValidCards = function (hand) {
    // first check the length of the played Cards. If its 0 then return the hand
    if (_this.length == 0) return hand;

    // Lets get the  suit of the played Card
    var suit = _this[0].card.suit;

    // If the player doesn't have the same suit, then return the hand
    if (hand.findIndex(function (card) {
      return card.suit === suit;
    }) == -1) return hand;

    // else return the cards with same suit
    return hand.filter(function (card) {
      return card.suit == suit;
    });
  };
  this.getTrickArray = function () {
    var trick = [];
    for (var i = 0; i < _this.length; i++) {
      trick.push(_this[i]);
    }
    return trick;
  };
}
Object.setPrototypeOf(Trick.prototype, Array.prototype);

},{}],8:[function(require,module,exports){
"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = Tricks;
var _trick = _interopRequireDefault(require("./trick.js"));
var _deck = require("./deck.js");
function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }
function _toConsumableArray(arr) { return _arrayWithoutHoles(arr) || _iterableToArray(arr) || _unsupportedIterableToArray(arr) || _nonIterableSpread(); }
function _nonIterableSpread() { throw new TypeError("Invalid attempt to spread non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method."); }
function _unsupportedIterableToArray(o, minLen) { if (!o) return; if (typeof o === "string") return _arrayLikeToArray(o, minLen); var n = Object.prototype.toString.call(o).slice(8, -1); if (n === "Object" && o.constructor) n = o.constructor.name; if (n === "Map" || n === "Set") return Array.from(o); if (n === "Arguments" || /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(n)) return _arrayLikeToArray(o, minLen); }
function _iterableToArray(iter) { if (typeof Symbol !== "undefined" && iter[Symbol.iterator] != null || iter["@@iterator"] != null) return Array.from(iter); }
function _arrayWithoutHoles(arr) { if (Array.isArray(arr)) return _arrayLikeToArray(arr); }
function _arrayLikeToArray(arr, len) { if (len == null || len > arr.length) len = arr.length; for (var i = 0, arr2 = new Array(len); i < len; i++) { arr2[i] = arr[i]; } return arr2; }
function Tricks(deck) {
  var _this = this;
  if (deck) {
    this.deck = deck;
  }
  this.trump = {
    set setTrump(value) {
      if ([].concat(_toConsumableArray(deck.suits), [undefined]).indexOf(value) != -1) this.trump = value;else throw TypeError("Trump should be valid");
    },
    get getTrump() {
      return this.trump;
    }
  };
  this.flip = {
    set setFlip(value) {
      if (typeof value == "boolean") this.flip = value;else throw TypeError("flip should be a boolean");
    },
    get getFlip() {
      return this.flip;
    }
  };
  this.maxLength = {
    set setMaxLength(value) {
      if (typeof value === "number") this.maxLength = value;else throw TypeError("maxLength should be number");
    },
    get getMaxLength() {
      return this.maxLength;
    }
  };

  // for adding the new trick into tricks
  this.addTrick = function () {
    var trick = new _trick["default"]();
    if (_this.flip.getFlip) trick.flip.setFlip = _this.flip.getFlip;else trick.flip.setFlip = false;
    _this.push(trick);
  };
  this.play = function (_ref) {
    var player = _ref.player,
      card = _ref.card;
    var length = _this.length;

    // if the length of the tricks is zero or
    // the length of lastly added trick is equals to the player count
    // we are adding new Trick to the tricks array
    if (length == 0 || _this[length - 1].length == _this.maxLength.getMaxLength) {
      _this.addTrick();
      //updating the length
      length = _this.length;
    }
    _this[length - 1].play({
      player: player,
      card: card
    });
    if (_this[length - 1].length == _this.maxLength.getMaxLength) return _this.getWinningCard();
    return;
  };
  this.undo = function () {
    if (_this.length < 0) return;
    if (_this[_this.length - 1].length > 0) return _this[_this.length - 1].pop();else return _this[_this.length - 2].pop();
  };
  this.getValidCards = function (hand) {
    if (_this.length > 0) return _this[_this.length - 1].getValidCards(hand);else return hand;
  };
  this.getWinningCard = function (trickNumber) {
    //(eg) trickNumber = 1 means first trick
    if (_this.length == 0) return;
    var deck = new _deck.Deck();
    var suit;
    var trick;
    if (trickNumber && trickNumber <= _this.length && _this[trickNumber - 1].length > 0) {
      suit = _this[trickNumber - 1][0].card.suit;
      trick = _this[trickNumber - 1].getTrickArray();
    } else if (_this[_this.length - 1].length !== _this.maxLength.getMaxLength) {
      suit = _this[_this.length - 2][0].card.suit;
      trick = _this[_this.length - 2].getTrickArray();
    } else {
      suit = _this[_this.length - 1][0].card.suit;
      trick = _this[_this.length - 1].getTrickArray();
    }
    if (_this.trump.getTrump !== undefined) {
      var trumpCards = trick.filter(function (x) {
        return x.card.suit == _this.trump.getTrump;
      });
      if (trumpCards.length == 1) {
        return trumpCards[0];
      }
      if (trumpCards.length > 0) {
        trumpCards.sort(function (card1, card2) {
          return deck.compare(card1.card, card2.card);
        });
        if (_this.flip.getFlip) {
          return trumpCards[trumpCards.length - 1];
        } else return trumpCards[0];
      }
    }
    var sameSuit = trick.filter(function (x) {
      return x.card.suit == suit;
    });
    if (sameSuit.length == 1) {
      return trick[0];
    }
    sameSuit.sort(function (card1, card2) {
      return deck.compare(card1.card, card2.card);
    });
    if (_this.flip.getFlip) {
      return sameSuit[sameSuit.length - 1];
    } else return sameSuit[0];
  };
  this.getTricksArray = function () {
    var tricks = [];
    for (var i = 0; i < _this.length; i++) {
      tricks.push(_this[i].getTrickArray());
    }
    return tricks;
  };

  //Making the trick from array
  this.loadArray = function (tricks) {
    if (tricks === undefined || tricks.length === 0) return;
    tricks.forEach(function (trick) {
      trick.forEach(function (card) {
        _this.play(card);
      });
    });
  };
}
Object.setPrototypeOf(Tricks.prototype, Array.prototype);

},{"./deck.js":1,"./trick.js":7}]},{},[2]);
